<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
  $router->post('process_request', ['uses' => 'APIController@process_request']);
  $router->get('check_license', ['uses' => 'APIController@check_license']);
  $router->get('activate_license', ['uses' => 'APIController@activate_license']);
  $router->get('deactivate_license', ['uses' => 'APIController@deactivate_license']);
  $router->get('test', ['uses' => 'APIController@test']);
  $router->get('return_url', ['uses' => 'APIController@return_url']);
  $router->get('test_authorise', ['uses' => 'APIController@test_authorise']);
  $router->get('save_tsheets', ['uses' => 'APIController@save_tsheets']);
  $router->get('refresh_tokens', ['uses' => 'APIController@refresh_tokens']);
  $router->get('version', ['uses' => 'APIController@version']);
  $router->get('get_client', ['uses' => 'APIController@get_client']);
  $router->get('get_tsheets_status', ['uses' => 'APIController@get_tsheets_status']);
});