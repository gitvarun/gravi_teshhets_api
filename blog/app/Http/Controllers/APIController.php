<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class APIController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    //woo commerce product id
    private $product_id = 235110;
    private $gravi_tsheets_wordpress_url = 'http://gravitsheets.com/';
    private $tsheets_api_url = 'https://rest.tsheets.com/api/v1/';
    private $gravi_tsheets_return_url = 'http://api.gravitsheets.com/api/return_url';

    public function version(Request $request) {
        phpinfo();
    }

    public function get_client(Request $request) {
        $tsheets_admin = DB::table('tsheets_admin')->first();
        return response()->json(['client_id' => Crypt::decrypt($tsheets_admin->client_id)]);
    }
    
    public function get_tsheets_status(Request $request) {
        $domain = $request->input('url');
        $tsheet = DB::table('tsheets')->where('domain',$domain)->first();
        $status = 0;
        if(!is_null($tsheet))
        {
            if(strlen($tsheet->access_token) > 0)
            {
                $status = 1;
            }
        }
        
        return response()->json(['status' => $status]);
    }
    

    public function test(Request $request) {
        //  $client_id = Crypt::encrypt('da231359872642f5ab8edc502f45505c');
        //  $secret = Crypt::encrypt('6fd951ed9e8f4ad58485cb170bf6370e');
 
        //  DB::table('tsheets_admin')->insert(['client_id' => $client_id, 'secret' => $secret]);
 
        // $quote_data = DB::table('licenses')
        //                 ->where('id', '=', 1)
        //                 ->first();
        // $url = $request->url();
        // return response()->json(['msg' => var_dump($request)]);
        return view('response');
    }



    public function return_url(Request $request) {

        $response_status = false;
        $code = $request->input('code');
        $domain = $request->input('state');

        $tsheet = DB::table('tsheets')
                ->where('domain', '=', $domain)
                ->first();

        $tsheet_admin = DB::table('tsheets_admin')->first();

        if (!is_null($tsheet)) {
            $request_body = array();
            $request_body['grant_type'] = 'authorization_code';
            $request_body['client_id'] = Crypt::decrypt($tsheet_admin->client_id);
            $request_body['client_secret'] = Crypt::decrypt($tsheet_admin->secret);
            $request_body['code'] = $code;
            $request_body['redirect_uri'] = $this->gravi_tsheets_return_url;

            try {
                $client = new Client(['base_uri' => $this->tsheets_api_url,]);
                $response = $client->post('grant', [
                    'debug' => FALSE,
                    'form_params' => $request_body,
                    'headers' => array('Authorization' => "Bearer {$code}")
                ]);

                $json_response = json_decode($response->getBody()->getContents(), true);

                $access_token = $json_response['access_token'];
                $refresh_token = $json_response['refresh_token'];


                $updated_time = date('Y-m-d H:i:s');
                DB::table('tsheets')
                        ->where('domain', '=', $domain)
                        ->update(['code' => Crypt::encrypt($code), 'updated_time' => $updated_time, 'access_token' => Crypt::encrypt($access_token), 'refresh_token' => Crypt::encrypt($refresh_token)]);
                $response_status = true;
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                
            } catch (\GuzzleHttp\Exception\ConnectException $e) {
                
            } catch (\GuzzleHttp\Exception\RequestException $e) {
                
            } catch (\GuzzleHttp\Exception\ServerException $e) {
                
            }
        }
        if($response_status == true)
        {
                return view('response');
        }
        else
        {
            return view('error_response');
        }
    }

    public function refresh_tokens(Request $request) {


        $tsheets = DB::table('tsheets')
                ->where('refresh_token', '<>', '')
                ->get();
        $tsheets_admin = DB::table('tsheets_admin')->first();

        foreach ($tsheets as $tsheet) {
            $request_body = array();
            $request_body['grant_type'] = 'refresh_token';
            $request_body['client_id'] = Crypt::decrypt($tsheets_admin->client_id);
            $request_body['client_secret'] = Crypt::decrypt($tsheets_admin->secret);
            $request_body['refresh_token'] = Crypt::decrypt($tsheet->refresh_token);
            $my_access_token = Crypt::decrypt($tsheet->access_token);
            $client = new Client(['base_uri' => $this->tsheets_api_url,]);
            $response = $client->post('grant', [
                'debug' => FALSE,
                'form_params' => $request_body,
                'headers' => array('Authorization' => "Bearer {$my_access_token}")
            ]);
            $json_response = json_decode($response->getBody()->getContents(), true);
            $access_token = $json_response['access_token'];
            $refresh_token = $json_response['refresh_token'];

            $updated_time = date('Y-m-d H:i:s');
            DB::table('tsheets')
                    ->where('domain', '=', $tsheet->domain)
                    ->update(['updated_time' => $updated_time, 'access_token' => Crypt::encrypt($access_token),
                        'refresh_token' => Crypt::encrypt($refresh_token)]);
        }

        return response()->json(['msg' => 'Refresh OK.']);
    }

    public function check_license(Request $request) {
        $plugin_license_key = $request->input('license_key');
        $url = $request->input('url');

        $check_response = $this->check_my_license($plugin_license_key, $url);

        return response()->json(['msg' => $check_response]);
    }

    public function activate_license(Request $request) {
        $plugin_license_key = $request->input('license_key');
        $url = $request->input('url');

        $licesne_data = DB::table('licenses')
                ->where('license_key', '=', $plugin_license_key)
                ->where('domain', '=', $url)
                ->first();

        $status = 'no status';

        if ($licesne_data != null) {
            return response()->json(['msg' => 'Licesne already in use.']);
        }

        if ($licesne_data == null) {
            //delete old if exists
            DB::table('tsheets')->where('domain', $url)->delete();
            DB::table('tsheets')->insert(['domain' => $url, 'code' => '', 'call_count' => 0, 'access_token' => '', 'refresh_token' => '']);

            $api_request = $this->make_get_get_license_api_call('status', $plugin_license_key);
            $dd = $api_request;

            if ($dd['success'] == false) {
                $status = 'API Success Error. Please Try Later';
            } else if ($dd['success'] == true) {
                $status = $dd['status_check'];
                if ($status == 'inactive') {
                    $api_request = $this->make_get_get_license_api_call('activate', $plugin_license_key);
                    DB::table('licenses')->insert(['license_key' => $plugin_license_key, 'domain' => $url, 'status' => 1]);
                }
            }
        }


        return response()->json(['msg' => 'Y']);
    }

    public function deactivate_license(Request $request) {
        $plugin_license_key = $request->input('license_key');
        $url = $request->input('url');

        DB::table('tsheets')->where('domain', $url)->delete();

        $licesne_data = DB::table('licenses')
                ->where('license_key', '=', $plugin_license_key)
                ->where('domain', '=', $url)
                ->first();

        $status = 'no status';

        if ($licesne_data != null) {
            DB::table('licenses')->where('license_key', $plugin_license_key)->where('domain', $url)->delete();
            $api_request = $this->make_get_get_license_api_call('status', $plugin_license_key);

            $dd = $api_request;

            if ($dd['success'] == false) {
                $status = 'API Success Error. Please Try Later';
            } else if ($dd['success'] == true) {
                $status = $dd['status_check'];
                if ($status == 'active') {
                    $api_request = $this->make_get_get_license_api_call('deactivate', $plugin_license_key);
                }
            }
        }


        return response()->json(['msg' => 'Y']);
    }

    private function check_my_license($plugin_license_key, $url) {
        $licesne_data = DB::table('licenses')
                ->where('license_key', '=', $plugin_license_key)
                ->where('domain', '=', $url)
                ->first();

        if (is_null($licesne_data)) {
            return 'false';
        }
        return 'true';
    }

    private function make_get_get_license_api_call($request, $license_key) {

        $instance = 'gravitsheets';
        if (strlen($license_key) > 10) {
            $instance = substr($license_key, 0, 5);
        }
        $client = new Client(['base_uri' => $this->gravi_tsheets_wordpress_url . '?wc-api=wc-am-api&request=' . $request . '&product_id=' .
            $this->product_id . '&instance=' . $instance . '&api_key=' . $license_key,]);
        $response = $client->get('', [
            'debug' => FALSE
        ]);

        if ($response->getStatusCode() == 200) {
            return json_decode($response->getBody()->getContents(), true);
        }
        return false;
    }

    public function process_request(Request $request) {
        $plugin_license_key = $request->input('license_key');
        $url = $request->input('url');

        if ($this->check_my_license($plugin_license_key, $url) == 'false') {
            return response()->json(['msg' => 'License Invalid']);
        }

        $tsheets = DB::table('tsheets')->where('domain', '=', $url)->first();
        if (is_null($tsheets)) {
            return response()->json(['msg' => 'tsheets token not found']);
        }
        $tsheets_token = Crypt::decrypt($tsheets->access_token);

        $tsheets_admin = DB::table('tsheets_admin')->first();

        $tsheets_calendar_id = $this->get_tsheets_calendar_id($tsheets_token);

        if ($tsheets_calendar_id == '') {
            return response()->json(['msg' => 'Invalid TSheets Token or no calendar defined']);
        }

        $tsheets_title = $request->input('title');
        $tsheets_location = $request->input('location');
        $tsheets_time_zone = $request->input('time_zone');
        $tsheets_all_day = $request->input('all_day');
        $tsheets_ed = $request->input('end_date');
        $tsheets_st = $request->input('start_time');
        $tsheets_et = $request->input('end_time');
        $tsheets_end_color = $request->input('end_color');
        $tsheets_end_suffix = $request->input('end_suffix');
        $tsheets_notes = $request->input('notes');

        //optional fields
        $tsheets_sd = $request->input('start_date');
        $tsheets_spt = $request->input('special_time');
        $tsheets_end_spt = $request->input('special_time_end');
        $tsheets_all_day_special = $request->input('all_day_special');
        $tsheets_start_color = $request->input('start_color');
        $tsheets_start_suffix = $request->input('start_suffix');

        $tsheets_special_is_after_day = $request->input('special_is_after_day');
        $tsheets_special_after_day_count = $request->input('special_after_day_count');

        if (is_null($tsheets_start_color)) {
            $tsheets_start_color = $tsheets_end_color;
        }

        if (is_null($tsheets_time_zone)) {
            $tsheets_time_zone = 'UTC';
        }

        $requests = array();
        //create first request body....
        $tsheets_start_datetime = $this->get_tsheets_date_time($tsheets_ed, $tsheets_st, 0, $tsheets_time_zone, $tsheets_all_day);
        $tsheets_end_datetime = $this->get_tsheets_date_time($tsheets_ed, $tsheets_et, 0, $tsheets_time_zone, $tsheets_all_day);

        $requests[count($requests)] = $this->build_tsheet_request_body($tsheets_calendar_id, $tsheets_start_datetime, $tsheets_end_datetime, $this->format_event_title($tsheets_title, $tsheets_end_suffix), $tsheets_location, $tsheets_notes, $tsheets_end_color, $tsheets_time_zone, $tsheets_all_day);


        //if optionl fields defined...create second event..
        if (($tsheets_sd != '' && $tsheets_special_is_after_day == 0) || $tsheets_special_is_after_day == 1) {
            $should_create_event = true;
            if ($tsheets_all_day != 1) {
                //not full day event
                if ($tsheets_spt == '' || $tsheets_end_spt == '') {
                    $should_create_event = false;
                }
            }

            if ($should_create_event == true) {
                $tsheets_start_datetime = $this->get_special_tsheets_date_time($tsheets_special_is_after_day, $tsheets_special_after_day_count, $tsheets_ed, $tsheets_sd, $tsheets_spt, 0, $tsheets_time_zone, $tsheets_all_day_special);
                $tsheets_end_datetime = $this->get_special_tsheets_date_time($tsheets_special_is_after_day, $tsheets_special_after_day_count, $tsheets_ed, $tsheets_sd, $tsheets_end_spt, 0, $tsheets_time_zone, $tsheets_all_day_special);

                $requests[count($requests)] = $this->build_tsheet_request_body($tsheets_calendar_id, $tsheets_start_datetime, $tsheets_end_datetime, $this->format_event_title($tsheets_title, $tsheets_start_suffix), $tsheets_location, $tsheets_notes, $tsheets_start_color, $tsheets_time_zone, $tsheets_all_day);
            }
        }

        $success_count = 0;
        foreach ($requests as $request_body) {
            if ($this->send_tsheets_post_request($request_body, $tsheets_token)) {
                $success_count++;
            }
        }

        return response()->json(['msg' => 'Success.' . $success_count . ' event(s) added']);
    }

    private function format_event_title($title, $suffix) {
        if (strlen(trim($suffix)) > 0) {
            $title = $title . ' - ' . $suffix;
        }
        return $title;
    }

    private function get_tsheets_calendar_id($tsheets_token) {
        $tsheets_cal_id = '';
        try {
            $request_body = array();
            $request_body['per_page'] = 1;
            $request_body['page'] = 1;
            
            $client = new Client(['base_uri' => $this->tsheets_api_url,]);
            $response = $client->get('schedule_calendars', [
                'debug' => FALSE,
                'form_params' => $request_body,
                'headers' => array('Authorization' => "Bearer {$tsheets_token}")
            ]);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {

            return $tsheets_cal_id;
        } catch (\GuzzleHttp\Exception\ClientException $e) {

            return $tsheets_cal_id;
        } catch (\GuzzleHttp\Exception\ConnectException $e) {

            return $tsheets_cal_id;
        } catch (\GuzzleHttp\Exception\RequestException $e) {

            return $tsheets_cal_id;
        } catch (\GuzzleHttp\Exception\ServerException $e) {

            return $tsheets_cal_id;
        }

        if ($response->getStatusCode() == 200) {
            $dd = json_decode($response->getBody()->getContents(), true);

            if ($dd != null) {
                if ($dd['results'] != null) {
                    if ($dd['results']['schedule_calendars'] != null) {
                        foreach ($dd['results']['schedule_calendars'] as $call) {
                            $tsheets_cal_id = $call['id'];
                            break;
                        }
                    }
                }
            }
        }
        return $tsheets_cal_id;
    }

    private function send_tsheets_post_request($request_body, $tsheets_token) {


        $client = new Client(['base_uri' => $this->tsheets_api_url,]);
        $response = $client->post('schedule_events', [
            'debug' => FALSE,
            'form_params' => $request_body,
            'headers' => array('Authorization' => "Bearer {$tsheets_token}")
        ]);

        if ($response->getStatusCode() == 200) {
            return true;
        }
        return false;
    }

    private function build_tsheet_request_body($tsheets_calendar_id, $tsheets_start_datetime, $tsheets_end_datetime, $tsheets_title, $tsheets_location, $tsheets_notes, $color, $tsheets_time_zone, $tsheets_all_day) {
        $request_body = array();
        $request_body['schedule_calendar_id'] = $tsheets_calendar_id;
        $request_body['start'] = $tsheets_start_datetime;
        $request_body['end'] = $tsheets_end_datetime;
        $request_body['draft'] = true;
        $request_body['active'] = true;
        $request_body['timezone'] = $tsheets_time_zone;
        $request_body['title'] = $tsheets_title;
        $request_body['location'] = $tsheets_location;
        $request_body['notes'] = $tsheets_notes;
        $request_body['color'] = $color;
        if ($tsheets_all_day == 1) {
            $request_body['all_day'] = true;
        }

        $r_b = array();
        $r_b[0] = $request_body;
        $request_body_data = array();
        $request_body_data['data'] = $r_b;
        $request_body_data['team_events'] = "base";
        return $request_body_data;
    }

    private function get_tsheets_date_time($tsheets_sd, $tsheets_st, $add_hours, $tsheets_time_zone, $tsheets_all_day) {
        $my_formatted_dt = '';
        $hour_offset = $this->get_time_zone_offset($tsheets_time_zone);
        if ($tsheets_all_day == 1) {
            return $tsheets_sd . 'T08:00:00' . $hour_offset;
        }
        $t_array = explode(" ", $tsheets_st);

        if (count($t_array) == 2) {
            $t_array1 = explode(":", $t_array[0]);
            if (count($t_array1) == 2) {
                $hour = $t_array1[0];
                $minute = $t_array1[1];
                if (strtoupper($t_array[1]) == 'PM') {
                    $hour += 12;
                }
                $hour += $add_hours;
                $hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
                $minute = str_pad($minute, 2, '0', STR_PAD_LEFT);
                $my_formatted_dt = $tsheets_sd . 'T' . $hour . ':' . $minute . ':00' . $hour_offset;
            }
        }
        return $my_formatted_dt;
    }

    private function get_special_tsheets_date_time($is_after_days, $day_count, $primary_tsheets_sd, $tsheets_sd, $tsheets_st, $add_hours, $tsheets_time_zone, $tsheets_all_day) {

        if ($is_after_days == 1) {
            $tsheets_sd = $primary_tsheets_sd;
            $tsheets_sd = date("Y-m-d", strtotime($tsheets_sd . ' + ' . $day_count . ' days'));
        }

        $my_formatted_dt = '';
        $hour_offset = $this->get_time_zone_offset($tsheets_time_zone);
        if ($tsheets_all_day == 1) {
            return $tsheets_sd . 'T08:00:00' . $hour_offset;
        }
        $t_array = explode(" ", $tsheets_st);

        if (count($t_array) == 2) {
            $t_array1 = explode(":", $t_array[0]);
            if (count($t_array1) == 2) {
                $hour = $t_array1[0];
                $minute = $t_array1[1];
                if (strtoupper($t_array[1]) == 'PM') {
                    $hour += 12;
                }
                $hour += $add_hours;
                $hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
                $minute = str_pad($minute, 2, '0', STR_PAD_LEFT);
                $my_formatted_dt = $tsheets_sd . 'T' . $hour . ':' . $minute . ':00' . $hour_offset;
            }
        }
        return $my_formatted_dt;
    }

    private function get_time_zone_offset($tsheets_time_zone) {
        $my_time = '';
        if ($tsheets_time_zone == 'tsPT') {
            $tsheets_time_zone = 'PST';
        } else if ($tsheets_time_zone == 'tsMT') {
            $tsheets_time_zone = 'MST';
        } else if ($tsheets_time_zone == 'tsAZ') {
            $tsheets_time_zone = 'MST';
        } else if ($tsheets_time_zone == 'tsCT') {
            $tsheets_time_zone = 'CST';
        } else if ($tsheets_time_zone == 'tsET') {
            $tsheets_time_zone = 'EST';
        }

        $dateTimeZoneUTC = new \DateTimeZone("UTC");
        $dateTimeZoneSelected = new \DateTimeZone($tsheets_time_zone);

        $dateTimeUTC = new DateTime("now", $dateTimeZoneUTC);
        $seconds = $dateTimeZoneSelected->getOffset($dateTimeUTC);

        if ($seconds < 0) {
            $seconds = abs($seconds);
            $hours = floor($seconds / 3600);
            $minutes = floor(($seconds / 60) % 60);
            $hours = str_pad($hours, 2, '0', STR_PAD_LEFT);
            $minutes = str_pad($minutes, 2, '0', STR_PAD_LEFT);
            $my_time = '-' . $hours . ':' . $minutes;
        } else {
            $hours = floor($seconds / 3600);
            $minutes = floor(($seconds / 60) % 60);
            $hours = str_pad($hours, 2, '0', STR_PAD_LEFT);
            $minutes = str_pad($minutes, 2, '0', STR_PAD_LEFT);
            $my_time = '+' . $hours . ':' . $minutes;
        }
        return $my_time;
    }

}
