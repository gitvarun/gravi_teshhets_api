<?php

namespace App\Console;


use DB;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use DateTime;
use Illuminate\Support\Facades\Crypt;
use GuzzleHttp\Client;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];
private $tsheets_api_url = 'https://rest.tsheets.com/api/v1/';
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $this->refresh_tokens();
        })->daily();
    }
    
        public function refresh_tokens() {
        
        $tsheets = DB::table('tsheets')
                        ->where('refresh_token', '<>', '')
                        ->get();
         $tsheets_admin = DB::table('tsheets_admin')->first();
         
        foreach($tsheets as $tsheet)
        {
            try
            {
             $request_body = array();
             $request_body['grant_type'] = 'refresh_token';
             $request_body['client_id'] = Crypt::decrypt($tsheets_admin->client_id);
             $request_body['client_secret'] = Crypt::decrypt($tsheets_admin->secret);
             $request_body['refresh_token'] = Crypt::decrypt($tsheet->refresh_token);
             $my_access_token = Crypt::decrypt($tsheet->access_token);
             $client = new Client(['base_uri' => $this->tsheets_api_url,]);
            $response = $client->post('grant', [
                'debug' => FALSE,
                'form_params' => $request_body,
                'headers' => array('Authorization' => "Bearer {$my_access_token}")
            ]);   
             if ($response->getStatusCode() == 200) {
                    $json_response  = json_decode($response->getBody()->getContents(), true);
                    $access_token = $json_response['access_token'];
                    $refresh_token = $json_response['refresh_token'];
                    
                    $updated_time = date('Y-m-d H:i:s');
                    DB::table('tsheets')
                    ->where('domain','=',$tsheet->domain)
                    ->update(['updated_time' => $updated_time,'access_token' => Crypt::encrypt($access_token),'refresh_token' => Crypt::encrypt($refresh_token)]); 
                    DB::table('callback')->insert(['code' => Crypt::encrypt($access_token),'state' => Crypt::encrypt($refresh_token)]);
               }
            }
            catch (\GuzzleHttp\Exception\BadResponseException $e) {}
            catch (\GuzzleHttp\Exception\ClientException $e) {}
            catch (\GuzzleHttp\Exception\ConnectException $e) {}
            catch (\GuzzleHttp\Exception\RequestException $e) {}
            catch (\GuzzleHttp\Exception\ServerException $e) {}
            catch (\Exception $exc) {}

        }
    }
}
