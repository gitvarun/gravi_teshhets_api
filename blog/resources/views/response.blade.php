<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GraviTsheets | Connecting Gravity Forms and TSheets</title>
        <link rel="icon" href="https://gravitsheets.com/wp-content/uploads/2019/03/cropped-site-icon-png-32x32.png" sizes="32x32" />
    </head>
    <body>
    <div style="margin-top:50px;text-align:center;">
    <a href="https://gravitsheets.com/">
		<img src="https://gravitsheets.com/wp-content/uploads/2019/01/gravitsheets-logo.png" alt="GraviTsheets" id="logo" data-height-percentage="54" data-actual-width="260" data-actual-height="30">
	</a>
    </div>
<div>
    <div style="background-color:lightgrey;font-family:arial;height:300px;padding:50px;margin-top:10px;text-align:center;">
        <h2>Your TSheets account is successfully integrated with your GraviTsheets plugin installation.</h2><br><br>
        <h2>You can close this browser tab now.</h2>
    </div>
</div>
    </body>
</html>

